// Package transport use segread and segwrite to copy data from online http.Response.Bordy (io.Reader)
// to SegWriter
// SegTransPorter need SegReader and SegWriter. Results be passed into Progresser
// SegReader needs: sidx(uint(8), seglog, seglog file(io.ReadWriteSeeker), srcurl (string) and DownloaderConfig
// SeglogWriter needs: sidx(uint(8), seglog, seglog file(io.ReadWriteSeeker) and output file(io.ReadWriteSeeker)
package transport

import (
	"segdown/display"
	"segdown/manager"
)

// SegTransporter use SegReader and SegWriter in same time
// one gorutine for one segment range
type SegTransporter interface {
	Start(schan chan manager.State, sw display.Progresser)
}
