package swrite

// SegWriter write data from SegReader into output file and
// seglog file using for resume and progress write to output
type SegWriter interface {
	//Pidx (pidx) number
	Pidx() uint8
	//Filename return path to writte downloaded file
	Filename() (fpath string)
	//Open open Filename() and SeglogFileName
	Open() error
	//Write (io.Writer) write chunk of data and seglog/segview changes
	Write(data []byte) (n int, err error)
	//Close (io.Closer) close output file and SeglogFile if is done, failed or cancel
	Close() error
}
