package sread

// SegReader connect to http source file and make io.Reader from http
// range header response.Body() by NumSegRange() from seglog values
type SegReader interface {
	//Pidx (pidx) number
	Pidx() uint8
	//Src URL
	Src() (uri string)
	//Connect return erro if fail get http.Response.Body()
	Connect() (err error)
	//Read - io.Reader read chunk of data from http.Response.Body()
	Read(data []byte) (n int, err error)
	//Close - http.Response.Body() after done, fail or cancel
	Close() error
}
