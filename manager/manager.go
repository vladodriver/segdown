// Package manager ..
package manager

// State of one downloaded segment process
type State uint8

const (
	//Inactive 0b000 (0) before start anything
	Inactive State = iota
	//Connecting 0b001 (1) when client try to connect and request range for segment
	Connecting
	//Retry 0b010 (2) when connection failed and will be retry connecting
	Retry
	//Running 0b011 (3) when transfered data from http.BodyReader into segment
	Running
	_
	//Failed 0b101 (5) when all connection retry failed and can not read data from http source
	Failed
	//Canceled 0b110 (6) when user stop downloading
	Canceled
	//Done 0b111 (7) when all segments download done
	Done
)

func (s State) String() string {
	names := []string{"Inactive", "Connecting", "Retry",
		"Running", "-", "Failed", "Canceled", "Done"}
	return names[s]
}

// SegChangeEv for segment events
type SegChangeEv struct {
	Pidx     uint8
	OldState State
	NewState State
}

// SegStatCounter ..
type SegStatCounter struct {
	parts,
	Inactive,
	Connecting,
	Retry,
	Running,
	Failed,
	Canceled,
	Done uint8
}

// NewSegStatCounter create new segment counter statistics
func NewSegStatCounter(n uint8) (ssc *SegStatCounter) {
	ssc = &SegStatCounter{parts: n}
	ssc.Inactive = n
	return ssc
}

// Change -
func (ssc *SegStatCounter) Change(scev SegChangeEv) {
	stmap := map[State]*uint8{
		Inactive:   &ssc.Inactive,
		Connecting: &ssc.Connecting,
		Retry:      &ssc.Retry,
		Running:    &ssc.Running,
		Failed:     &ssc.Failed,
		Canceled:   &ssc.Canceled,
		Done:       &ssc.Done,
	}

	stold := stmap[scev.OldState]
	stnew := stmap[scev.NewState]

	if *stold > 0 {
		*stold--
	}
	if *stnew < ssc.parts {
		*stnew++
	}
}

// IsDone return true if all segments is in terminal state
func (ssc *SegStatCounter) IsDone() bool {
	return ssc.Done+ssc.Canceled+ssc.Failed == ssc.parts
}
