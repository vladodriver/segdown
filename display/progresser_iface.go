package display

import (
	"segdown/manager"
	"time"
)

// Progresser interface for progress logic (for gui progress view)
type Progresser interface {
	//Bytes show number of downloaded bytes for segmend at pidx
	Bytes(pidx uint8, nb uint64)
	//State change state (color) by manager.State value for segmend at pidx
	State(pidx uint8, sst manager.State)
	//SegmentCounter
	SegmentsCounter(sc manager.SegStatCounter)
	//Monitor display already downloaded, speed and time remain statistics
	Monitor(downloaded uint64, avgspd uint64, instsod uint64, elasped time.Duration, remain time.Duration)
}
