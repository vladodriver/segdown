package display

import (
	"segdown/manager"
	"time"
)

// DisplayProgress -
type DisplayProgress struct {
	fsize  uint64 //downloaded filesisze
	outdir string //output directory
	fname  string //output filename
}

// NewDP create new DisplayProgress instance impl. of Progresser interface
func NewDP(fsize uint64, outdir, fname string) (dp *DisplayProgress) {
	return &DisplayProgress{fsize, outdir, fname}
}

// Bytes set number of downloaded bytes for segment at pidx
func (dp *DisplayProgress) Bytes(pidx uint8, nb uint64) {

}

// State change state (Color) for segment at pidx
func (dp *DisplayProgress) State(pidx uint8, sst manager.State) {

}

// Monitor update download statistics (bytes downloaded, avg. speed, inst. speed, elasped and remain time)
func (dp *DisplayProgress) Monitor(down uint64, avgsp uint64, instsp uint64, elasped time.Duration, remain time.Duration) {

}

//TODO implement TUI way for segment values layout (rows x columns), size of cells by numbers, bytesize units diwplay ...
