package seglog

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"math"
	"math/bits"
	"os"
)

// SegLog implement byte encoding aritmetic for make segments
type SegLog struct {
	fsize   uint64
	parts   uint8
	logfile *os.File
}

// NewSegLog --
func NewSegLog(fsize uint64, parts uint8) (sfs *SegLog, err error) {
	if uint64(parts) > fsize {
		return nil, fmt.Errorf("Segio err: %d filesize not possible diviide to %d parts", fsize, parts)
	}
	return &SegLog{fsize, parts, nil}, nil
}

// ResumeFrom io.ReadWriteSeeker (ex. *os.File)
func ResumeFrom(slogfile string) (sfs *SegLog, err error) {
	sfs = &SegLog{}
	if sfs.openLogFile(slogfile); err != nil {
		return nil, fmt.Errorf("Can not resume seglog from file: %s error: %v", slogfile, err)
	}
	if err = sfs.loadFilesize(sfs.logfile); err != nil {
		return nil, err
	}
	if err = sfs.loadParts(sfs.logfile); err != nil {
		return nil, err
	}
	return sfs, nil
}

// FileSize => sfs.fsize
func (sfs *SegLog) FileSize() (s uint64) { return sfs.fsize }

// Parts return number of part segments sfs.parts
func (sfs *SegLog) Parts() (s uint8) { return sfs.parts }

// Destroy seglog file after download is done
func (sfs *SegLog) Destroy() (err error) {
	return os.Remove(sfs.logfile.Name())
}

// NumSegRange return start, end byte of n-th (0 - (parts-1)) segment from parts
// if sidx is > (parts - 1) this will return last sidx
func (sfs *SegLog) NumSegRange(sidx uint8) (start uint64, end uint64) {
	psize := sfs.numSegSize()
	if sidx >= sfs.parts {
		sidx = sfs.parts - 1
	}
	start = uint64(sidx) * psize
	end = (psize * (uint64(sidx) + 1)) - 1
	if sidx == (sfs.parts - 1) {
		end = sfs.fsize - 1
	}
	return
}

// NumSegCap returm number of bytes for full capacity downloaded segment at sidx
func (sfs *SegLog) NumSegCap(sidx uint8) (cap uint64) {
	s, e := sfs.NumSegRange(sidx)
	return (e - s) + 1
}

// WriteStartData write all starting bytes to provided writer
func (sfs *SegLog) WriteStartData(slogfile string) (err error) {
	if sfs.logfile, err = os.Create(slogfile); err != nil {
		return fmt.Errorf("Write start data to seglog file: %s failed: %v", slogfile, err)
	}
	fbs := sfs.numFileByteSize()
	sbs := sfs.numSegByteSize()
	firstb := encodeHalfBytes(fbs, sbs)

	if n, err := sfs.logfile.Write(firstb); err != nil {
		return fmt.Errorf("Write filesize/segment sizes into seglog file failed: %v", err)
	} else if n != 1 {
		return fmt.Errorf("Write filesize/segment sizes into selog file wrote: %d bytes, but must be: %d", n, 1)
	}
	if n, err := sfs.logfile.Write(leUint64ToBytes(sfs.fsize, fbs)); err != nil {
		return fmt.Errorf("Write filesize into seglog file failed: %v", err)
	} else if uint8(n) != fbs {
		return fmt.Errorf("Write filesize number into seglog file wrote: %d bytes, but must be: %d", n, fbs)
	}

	for i := uint8(0); i < sfs.parts; i++ {
		if _, err := sfs.logfile.Write(leUint64ToBytes(0, sbs)); err != nil {
			return fmt.Errorf("Write start position bytes for idx: %d into seglog file failed: %v", i, err)
		}
	}

	bsa := sfs.numByteSizeAll()
	if n, err := sfs.logfile.Seek(0, io.SeekCurrent); err != nil {
		return fmt.Errorf("Can not seek back to to start in seglog file err: %v", err)
	} else if uint(n) != bsa {
		return fmt.Errorf("Size of all bytes writen is: %d must be: %d", n, bsa)
	}
	return nil
}

// SumDownloadedBytes sum of all bytes already downloaded on all segments
func (sfs *SegLog) SumDownloadedBytes() (sumall uint64, err error) {
	if sfs.logfile == nil {
		return 0, fmt.Errorf("Seglog file must be created and opened before sum all downloaded bytes")
	}
	//sum of all downloaded sizes
	for i := uint8(0); i < sfs.parts; i++ {
		var downseg uint64
		if downseg, err = sfs.ReadSegBytesNum(i); err != nil {
			return sumall, err
		}
		sumall += (downseg)
	}
	return sumall, nil
}

// ReadSegBytesNum read segment (at sidx) value as bytes to provided io.Reader
func (sfs *SegLog) ReadSegBytesNum(sidx uint8) (v uint64, err error) {
	if sfs.logfile == nil {
		return 0, fmt.Errorf("Seglog file must be created and opened before read values")
	}
	var sbs uint8
	if sbs, err = sfs.readySegPosRWSize(sidx, sfs.logfile); err != nil {
		return 0, err
	}
	valbytes := make([]byte, sbs)
	var nr int
	if nr, err = sfs.logfile.Read(valbytes); err != nil {
		return 0, fmt.Errorf("read byte value at segment index %d failed: %v", sidx, err)
	} else if uint8(nr) != sbs {
		return 0, fmt.Errorf("read of value of segment index: %d byte size read: %d != %d", sidx, nr, sbs)
	}
	return leBytesToUint64(valbytes), nil
}

// AddSegBytesNum write (vadd + last position) value to segment at sidx to bytes into rws io.Writer
func (sfs *SegLog) AddSegBytesNum(sidx uint8, vadd uint64) (err error) {
	if sfs.logfile == nil {
		return fmt.Errorf("Seglog file must be created and opened before add values")
	}
	var curdown uint64 //already downloaded in segment
	if curdown, err = sfs.ReadSegBytesNum(sidx); err != nil {
		return fmt.Errorf("Read current downloaded value fromm segment idx: %d failed: %v", sidx, err)
	}

	capmax := sfs.NumSegCap(sidx)
	newpos := curdown + vadd
	if newpos > capmax {
		return fmt.Errorf(`Segment idx: %d has downloaded: %d bytes. Download this segment is done at: %d bytes,
		 not possible write value: %d because overflow maximum: %d`,
			sidx, curdown, capmax, newpos, capmax)
	}
	var sbs uint8
	if sbs, err = sfs.readySegPosRWSize(sidx, sfs.logfile); err != nil {
		return err
	}
	valbw := leUint64ToBytes(newpos, sbs)
	var nw int
	if nw, err = sfs.logfile.Write(valbw); err != nil {
		return fmt.Errorf("write bytes value at segment index %d failed: %v", sidx, err)
	} else if uint8(nw) != sbs {
		return fmt.Errorf("write of value of segment index: %d byte size wroten: %d != %d", sidx, nw, sbs)
	}
	return nil
}

// openLogFile use for open old seglog file for resume segment download
func (sfs *SegLog) openLogFile(slogfile string) (err error) {
	if sfs.logfile, err = os.OpenFile(slogfile, os.O_RDWR, 0600); err != nil {
		return err
	}
	return nil
}

// numSegByteSize segment byte size 1 - 8 bytes (byte len of segment value)
func (sfs *SegLog) numSegByteSize() (sbs uint8) {
	lstart, lend := sfs.NumSegRange(sfs.parts - 1)
	segsize := sfs.numSegSize()
	var bitl int
	if segsize > (lend - lstart) {
		bitl = bits.Len64(segsize)
	} else {
		bitl = bits.Len64(lend - lstart)
	}
	return uint8(math.Ceil(float64(bitl) / 8))
}

// numByteSizeAll segment status file byte-size from non nul size
func (sfs *SegLog) numByteSizeAll() (bsa uint) {
	fbs := sfs.numFileByteSize()
	sbs := sfs.numSegByteSize()
	segments := uint(sbs) * uint(sfs.parts)
	return (1 + uint(fbs)) + segments
}

// numFileByteSize segment byte size 1 - 8 bytes (byte len of filesize value)
func (sfs *SegLog) numFileByteSize() (fbs uint8) {
	bitl := bits.Len64(sfs.fsize)
	return uint8(math.Ceil(float64(bitl) / 8))
}

// numSegSize optimal byte size of segment for num of (minimal) parts
func (sfs *SegLog) numSegSize() (s uint64) {
	if sfs.parts == 0 {
		sfs.parts = 1
	}
	ceilsize := uint64(math.Ceil(float64(sfs.fsize) / float64(sfs.parts)))
	floorsize := uint64(math.Floor(float64(sfs.fsize) / float64(sfs.parts)))
	//if ceilsize get overflov filesize before last segment
	if uint64(sfs.parts-1)*ceilsize > sfs.fsize {
		s = floorsize
		//floorsize for segment need more parts (possible add more)
		sizelast := (sfs.fsize - (uint64(sfs.parts)-1)*s)
		padd := uint8(math.Floor(float64(sizelast / s)))
		if (255 - sfs.parts) < padd {
			sfs.parts = 255
		} else {
			sfs.parts += padd
		}
	} else {
		s = ceilsize //use smaller ceilsize
	}
	return
}

// readSbsBytesVal read segmant byte size (in first byte) into uint8 number
func (sfs *SegLog) readSbsBytesVal(rws io.ReadWriteSeeker) (fbs, sbs uint8, err error) {
	if err = absSeekTo(0, rws); err != nil {
		return 0, 0, err
	}
	sbsb := make([]byte, 1)
	var nr int
	if nr, err = rws.Read(sbsb); err != nil {
		return 0, 0, fmt.Errorf("read segment byte size failed: %v", err)
	} else if nr != 1 {
		return 0, 0, fmt.Errorf("read segment byte size return: %db instead of: 1b", nr)
	}
	fbs, sbs = decodeHalfBytes(sbsb)
	return
}

// loadFilesize read filesize from rws tp sfs.fsize
func (sfs *SegLog) loadFilesize(rws io.ReadWriteSeeker) (err error) {
	var fbs uint8
	if fbs, _, err = sfs.readSbsBytesVal(rws); err != nil {
		return err
	}
	if err = absSeekTo(int64(1), rws); err != nil {
		return err
	}
	fsbb := make([]byte, int(fbs))
	var nr int
	if nr, err = rws.Read(fsbb); err != nil {
		return fmt.Errorf("read filesize bytes failed: %v", err)
	} else if nr != int(fbs) {
		return fmt.Errorf("read filesize return: %db instead of: %db", nr, fbs)
	}
	sfs.fsize = leBytesToUint64(fsbb)
	return nil
}

// loadParts determine number of parts from existing loaded rws using sbs and seek to end
func (sfs *SegLog) loadParts(rws io.ReadWriteSeeker) (err error) {
	var fbs, sbs uint8
	if fbs, sbs, err = sfs.readSbsBytesVal(rws); err != nil {
		return err
	}

	var segfend int64
	if segfend, err = rws.Seek(0, io.SeekEnd); err != nil {
		return err
	}

	sfs.parts = uint8((segfend - 1 - int64(fbs)) / int64(sbs))
	return nil
}

// seek to segment index pos. and return read/write byte size before read/write value segment at sidx
func (sfs *SegLog) readySegPosRWSize(sidx uint8, rws io.ReadWriteSeeker) (sbs uint8, err error) {
	var fbs uint8
	if fbs, sbs, err = sfs.readSbsBytesVal(rws); err != nil {
		return sbs, err
	}

	if sidx > (sfs.parts - 1) {
		return sbs, fmt.Errorf("Read or write to index: %d in seglog file is out of range for %d parts", sidx, sfs.parts-1)
	}

	//seekto := 1 + int64(sidx+1)*(int64(sbs))
	seekto := 1 + int64(fbs) + int64(sidx)*int64(sbs)
	if absSeekTo(seekto, rws); err != nil {
		return sbs, err
	}
	return sbs, nil
}

// absSeekTo seek any io.ReadWriteSeeker to seekto position
func absSeekTo(seekto int64, rws io.ReadWriteSeeker) (err error) {
	var seekpos int64
	if seekpos, err = rws.Seek(seekto, 0); err != nil {
		return fmt.Errorf("seek to position: %d: failed: %v", seekto, err)
	} else if seekto != seekpos {
		return fmt.Errorf("seek to position: %d != %d after seek", seekpos, seekto)
	}
	return nil
}

// leUint64ToBytes make bytes from uint64 Le if cut is 0, then byte len determine automatically
func leUint64ToBytes(ui uint64, size uint8) (b []byte) {
	if size > 8 {
		size = 8
	}
	buf := &bytes.Buffer{}
	binary.Write(buf, binary.LittleEndian, ui)
	return buf.Bytes()[:size]
}

// leBytesToUint64 make uint64 from bytes
func leBytesToUint64(b []byte) (ui uint64) {
	lb := len(b)
	for i := 0; i < (8 - lb); i++ {
		b = append(b, 0x00)
	}
	binary.Read(bytes.NewBuffer(b), binary.LittleEndian, &ui)
	return ui
}

// encodeHalfBytes --
func encodeHalfBytes(l, r uint8) (out []byte) {
	out = make([]byte, 1)
	outn := uint8((l * 16) + r)
	buf := &bytes.Buffer{}
	binary.Write(buf, binary.LittleEndian, outn)
	out = buf.Bytes()
	return out
}

// decodeHalfBytes --
func decodeHalfBytes(data []byte) (l, r uint8) {
	var bnum uint8
	rdr := bytes.NewReader(data)
	binary.Read(rdr, binary.LittleEndian, &bnum)
	r = bnum & 15
	l = (bnum >> 4) & 15
	return
}
