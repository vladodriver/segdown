package seglog

import (
	"math"
	"math/bits"
	"os"
	"path/filepath"
	"testing"
)

const (
	FSIZE = 7234567891
	PARTS = 100
	TFILE = "test.udown"
)

func tfpath() string {
	return filepath.Join(os.TempDir(), TFILE)
}

var tseglog *SegLog

func init() {
	// seglog for (fsize) bytes and (n) segments
	var err error
	if tseglog, err = NewSegLog(FSIZE, PARTS); err != nil {
		panic(err)
	}
}

func TestNumSegSize(t *testing.T) {
	w := uint64(math.Ceil(float64(FSIZE) / float64(PARTS)))
	if g := tseglog.numSegSize(); g != w {
		t.Errorf("Size of segment for %d parts for %d bytes file want: %d got : %d", PARTS, FSIZE, w, g)
	}
}

func TestNumSegRange(t *testing.T) {
	psize := tseglog.numSegSize()
	for sidx := uint8(0); sidx < uint8(PARTS); sidx++ {
		var wend uint64
		wstart := uint64(sidx) * psize
		if sidx == PARTS-1 {
			wend = tseglog.FileSize() - 1
		} else {
			wend = (psize * (uint64(sidx) + 1)) - 1
		}
		gstart, gend := tseglog.NumSegRange(sidx)
		if wstart != gstart {
			t.Errorf("Start positiong for segment idx: %d want: %d got: %d", sidx, wstart, gstart)
		} else if wend != gend {
			t.Errorf("End positiong for segment idx: %d want: %d got: %d", sidx, wend, gend)
		}
	}
}

func TestNumSegByteSize(t *testing.T) {
	fsize := tseglog.FileSize()
	bits := bits.Len64(fsize)
	want := uint8(math.Ceil(float64(bits) / 8))
	if got := tseglog.numFileByteSize(); got != want {
		t.Errorf("Byte size of segment for %d bytes filesize want: %d got: %d", fsize, want, got)
	}
}

func TestMumByteSizeAll(t *testing.T) {
	fbs := tseglog.numFileByteSize()
	sbs := tseglog.numSegByteSize()
	allseg := uint(sbs) * (uint(tseglog.Parts()))
	want := (1 + uint(fbs)) + allseg
	if got := tseglog.numByteSizeAll(); got != want {
		t.Errorf("File size: %d for %d parts want seglog filesize: %d got %d", tseglog.FileSize(), tseglog.Parts(), want, got)
	}
}

func TestWriteStartDataAll(t *testing.T) {
	if err := tseglog.WriteStartData(tfpath()); err != nil {
		t.Errorf("Error write seglog start data: %v", err)
	} else {
		if fsi, err := os.Stat(tfpath()); err != nil {
			t.Errorf("Can not get info about file: %s", tfpath())
		} else {
			gsize := fsi.Size()
			if wsize := tseglog.numByteSizeAll(); gsize != int64(wsize) {
				t.Errorf("Seglog file bytes written: %d want: %d", gsize, wsize)
			}
		}
	}
}

func TestLoadFileSize(t *testing.T) {
	if tf, err := os.OpenFile(tfpath(), os.O_RDONLY, 0400); err != nil {
		t.Errorf("Can not open test file %s", tfpath())
	} else {
		if terr := tseglog.loadFilesize(tf); terr != nil {
			t.Errorf("%v", terr)
		}
		got := tseglog.FileSize()
		if got != uint64(FSIZE) {
			t.Errorf("File size of test file: %s want: %d got: %d", tfpath(), FSIZE, got)
		}
	}
}

func TestLoadParts(t *testing.T) {
	if tf, err := os.OpenFile(tfpath(), os.O_RDONLY, 0400); err != nil {
		t.Errorf("Can not open test file %s", tfpath())
	} else {
		if terr := tseglog.loadParts(tf); err != nil {
			t.Errorf("%v", terr)
		}
		got := tseglog.Parts()
		want := uint8(PARTS)
		if got != want {
			t.Errorf("Loaded parts num from tet file: %s want: %d got %d", tfpath(), want, got)
		}
	}
}

func TestLeConversions(t *testing.T) {
	want := uint64(FSIZE)
	bn := leUint64ToBytes(want, 5)
	if got := leBytesToUint64(bn); got != want {
		t.Errorf("Conversion uint64 to bytes and back failed want: %d got: %d", want, got)
	}
}

func TestClean(t *testing.T) {
	if err := tseglog.Destroy(); err != nil {
		t.Error(err)
	}
}
