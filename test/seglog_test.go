package test

import (
	"os"
	"path/filepath"
	"segdown/seglog"
	"testing"
)

var tseglog *seglog.SegLog

const (
	FSIZE = 7234567891
	PARTS = 100
	TFILE = "test.udown"
)

func tfpath() string {
	return filepath.Join(os.TempDir(), TFILE)
}

func init() {
	// seglog for (fsize) bytes and (n) segments
	var err error
	if tseglog, err = seglog.NewSegLog(FSIZE, PARTS); err != nil {
		panic(err)
	}
	if err := tseglog.WriteStartData(tfpath()); err != nil {
		panic(err)
	}
}

func TestResumeFrom(t *testing.T) {
	var err error
	if tseglog, err = seglog.ResumeFrom(tfpath()); err != nil {
		t.Errorf("Load seglog for resume failed: %v", err)
	} else if tseglog.Parts() != PARTS || tseglog.FileSize() != FSIZE {
		t.Errorf("Fsize and parts is incorrect want: %d, %d got: %d. %d", FSIZE, 100, tseglog.FileSize(), tseglog.Parts())
	}
}

// This test testing also ReadSegBytesVal where called it
func TestSumDownBytesZero(t *testing.T) {
	if n, err := tseglog.SumDownloadedBytes(); err != nil {
		t.Errorf("Get sum of downloaded bytes failed err: %v", err)
	} else if n != 0 {
		t.Errorf("Downlaoded bytes for new seglog file must be 0 not: %d", n)
	}
}

func TestAddWriteSegBytesVal(t *testing.T) {
	var want uint64 = 0
	for i := uint64(0); i < PARTS; i++ {
		want += (100 + (i + 1))
	}

	for idx := uint8(0); idx < PARTS; idx++ {
		if err := tseglog.AddSegBytesNum(idx, uint64((100 + (idx + 1)))); err != nil {
			t.Errorf("Add %d bytes to segment index: %d failed with err: %v", (100 + (idx + 1)), idx, err)
		}
	}

	if sum, err := tseglog.SumDownloadedBytes(); err != nil {
		t.Errorf("Sum of all downloaded bytes failed err: %v", err)
	} else if sum != want {
		t.Errorf("Sum of all add downloaded bytes want: %d got: %d", want, sum)
	}
}

func TestFullDownloadedSegMustBeAtEnd(t *testing.T) {
	for idx := uint8(0); idx < uint8(PARTS); idx++ {
		if curv, err := tseglog.ReadSegBytesNum(idx); err != nil {
			t.Errorf("Failed get current value of segment idx: %d err: %v", idx, err)
		} else {
			//_, maxd := tseglog.NumSegByteSegSize()
			remadd := (tseglog.NumSegCap(idx) - curv)
			//t.Logf("Cur: %d + %d = %d at end", curv, remadd, to)
			if err := tseglog.AddSegBytesNum(idx, remadd); err != nil {
				t.Errorf("Write remain value: %d tp segment idx: %d failed err: %v", remadd, idx, err)
			}
		}
	}
	for idx := uint8(0); idx < PARTS; idx++ {
		if rv, err := tseglog.ReadSegBytesNum(idx); err != nil {
			t.Errorf("Failed read status value at iddex: %d after downlad end - err: %v", idx, err)
		} else if end := tseglog.NumSegCap(idx); end != rv {
			t.Errorf("Value of segment idx: %d must be at end: %d got: %d", idx, end, rv)
		}
	}
	if dwn, err := tseglog.SumDownloadedBytes(); err != nil {
		t.Errorf("Failed sum of all downloaded bytes after all done - err: %v", err)
	} else if dwn != uint64(FSIZE) {
		t.Errorf("After all downloads done sum want: %d, got: %d", FSIZE, dwn)
	}
}

func TestAddMoreBytesMustBeFailedAll(t *testing.T) {
	for idx := uint8(0); idx < PARTS; idx++ {
		//t.Logf("IDx :%d, err: %v", idx, err)
		if err := tseglog.AddSegBytesNum(idx, 1); err == nil {
			t.Errorf("Add +1 byte to download segment idx: %d must fail wuth error, but is :%v", idx, err)
		}
	}
}

func TestClean(t *testing.T) {
	if err := tseglog.Destroy(); err != nil {
		t.Error(err)
	}
}
